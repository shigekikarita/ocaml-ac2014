open Core.Std
open Core_bench.Std

open Lacaml.S


open Spoc
open Cublas
       
let devices = Spoc.Devices.init ()
let dev = ref devices.(0)
let x = 1
let auto_transfers = ref false  
  
let () =
  Cublas.init ();
  Random.self_init ();
  let a = Spoc.Vector.create Spoc.Vector.float32 (x * x)
  and b = Spoc.Vector.create Spoc.Vector.float32 (x * x)
  and res = Spoc.Vector.create Spoc.Vector.float32 (x * x) in
  for i = 0 to (Spoc.Vector.length a - 1) do
    Spoc.Mem.set a i (Random.float 32.);
    Spoc.Mem.set b i (Random.float 32.);
    Spoc.Mem.set res i (Random.float 32.);
  done;

  Spoc.Mem.auto_transfers !auto_transfers;
  if (not !auto_transfers) then
	begin
	  Printf.printf "Transfering Vectors (on Device memory)\n";
	  flush stdout;
	  Spoc.Mem.to_device a !dev;
	  Spoc.Mem.to_device b !dev;
	  Spoc.Mem.to_device res !dev;	    
	end; 
    
  let m = Mat.random x x in
  let n = Mat.random x x in
  Command.run (
      Bench.make_command [
          Bench.Test.create
            ~name:"id"
            (fun () -> ());

          Bench.Test.create
            ~name:"sgemm 4096 * 4096"
            (fun () -> ignore (gemm m n));

          Bench.Test.create
            ~name:"cublasSgemm 4096 * 4096"
            (fun () -> Cublas.cublasSgemm 'n' 'n' x x x 1.0 a x b x 0.0 res x !dev);

          Bench.Test.create
            ~name:"Transfering Vectors (on CPU memory)"
            (fun () -> Spoc.Mem.to_cpu res ())

            ]);

  Cublas.shutdown ();  
  Spoc.Devices.flush !dev ();
  
