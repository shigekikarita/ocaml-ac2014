open Lacaml.S
open Core.Std
          
(* serialize*)
let save_mat_sexps path mat_list =
  let to_sexp mat =
    Array.sexp_of_t (Array.sexp_of_t Float.sexp_of_t)
                    (Mat.to_array mat) in
  Sexp.save_sexps path (List.map mat_list to_sexp);;

(* deserialize *)
let load_mat_sexps path  =
  let to_mat sex =
    Mat.of_array (Array.t_of_sexp (Array.t_of_sexp Float.t_of_sexp) sex) in
  List.map (Sexp.load_sexps path) to_mat;;

