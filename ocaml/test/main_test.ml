open OUnit2
open Serialize
open Lacaml.S

let test_serialize test_ctxt =
  let a = Mat.random 12 3 in
  let b = Mat.random 1 7 in
  save_mat_sexps "test.scm" [a; b];
  let xs = load_mat_sexps "test.scm"in
  assert_equal xs [a; b]

let suite =
  "suite">:::
    [
      "test_serialize">:: test_serialize
    ]
           
let () = run_test_tt_main suite

